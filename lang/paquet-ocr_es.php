<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-ocr?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// O
	'ocr_description' => 'El plugin permite analizar un archivo imagen (por ejemplo un documento escaneado) para extraer el texto contenido. Utiliza la librería de OCR (Optical character recognition) [tesseract->http://code.google.com/p/tesseract-ocr/wiki/ReadMe].',
	'ocr_nom' => 'ocr',
	'ocr_slogan' => 'Extraer un texto de una imagen por OCR'
);
