<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/ocr.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// O
	'ocr_description' => 'Le plugin permet d’analyser un fichier image (typiquement un document scanné) pour extraire le texte contenu. Il utilise la librairie d’OCR (Optical character recognition) [tesseract->http://code.google.com/p/tesseract-ocr/wiki/ReadMe].',
	'ocr_nom' => 'ocr',
	'ocr_slogan' => 'Extraire un texte d’une image par OCR'
);
